﻿using System.Diagnostics;
using System.Windows.Forms;

namespace Taconator.CoCRP
{
    public partial class Main : Form
    {
        /// <summary>
        /// The actual CoC process
        /// </summary>
        Process Stalker;
        public Main()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Occurs when CoC is closed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void StalkerClosed(object sender, System.EventArgs e) => Cleanup();

        /// <summary>
        /// Disposes of the Rich Presence Controller and closes this application
        /// </summary>
        public void Cleanup()
        {
            IM.ControllerInstance.Dispose();
            Application.Exit();
        }

        /// <summary>
        /// Logs something to the listbox on this form
        /// </summary>
        /// <param name="Text">Text to log</param>
        public void Log(string Text) => IM.MainWindow.LogBox.Items.Add(Text);

        /// <summary>
        /// Load event for this form, adds this to the Instance Manager, creates a new <seealso cref="Controller"/> instance, detects CoC and starts it if necessary, then attaches the Exit event to it
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Main_Load(object sender, System.EventArgs e)
        {
            IM.MainWindow = this;
            IM.ControllerInstance = new Controller();
            if (!(Process.GetProcessesByName("xrEngine").Length > 1))
            {
                Log("CoC not open so opening it now");
                Process.Start(System.IO.Path.Combine(System.Environment.CurrentDirectory, "Stalker-CoC.exe"), "-dbg");
                while (Process.GetProcessesByName("xrEngine").Length < 1)
                {
                    System.Threading.Thread.Sleep(500);
                }
            }
            Stalker = Process.GetProcessesByName("xrEngine")[0];
            Stalker.EnableRaisingEvents = true;
            Stalker.Exited += StalkerClosed;
            Stalker.Disposed += StalkerClosed;
            Log("Got X-Ray Engine process and attached exit event");
        }

        /// <summary>
        /// Defunct
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void savesPathToolStripMenuItem_Click(object sender, System.EventArgs e) => Log(IM.ControllerInstance.RPFileWatcher.Path);

        /// <summary>
        /// Closes just this program, leaving CoC open
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitRPToolStripMenuItem_Click(object sender, System.EventArgs e) => Cleanup();

        /// <summary>
        /// Closes both this program and CoC
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitRPCoCToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            Stalker.Kill();
            Cleanup();
        }
    }
}
