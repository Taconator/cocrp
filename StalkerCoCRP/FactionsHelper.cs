﻿using System.Collections.Generic;
namespace Taconator.CoCRP
{
    /// <summary>
    /// Class for replacing the shortened or different faction names with their real names
    /// </summary>
    public static class FactionsHelper
    {
        /// <summary>
        /// Shortened or different faction names and their real names
        /// </summary>
        public static readonly Dictionary<string, string> Factions = new Dictionary<string, string>
        {
            { "ecolog", "ecologist" },
            { "stalker", "loner" },
            { "csky", "clear sky"},
            { "army", "military" },
            { "killer", "mercenary" },
            { "dolg", "duty" }
        };

        /// <summary>
        /// Same as accessing the dictionary directly, except with a method call
        /// </summary>
        /// <param name="FactionString">Original faction name</param>
        /// <returns>Fixed faction name</returns>
        public static string GetFaction(string FactionString) => (Factions.TryGetValue(FactionString, out string Faction)) ? Faction : FactionString;
    }
}