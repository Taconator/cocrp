﻿namespace Taconator.CoCRP
{
    /// <summary>
    /// Instance Manager
    /// </summary>
    public static class IM
    {
        public static Main MainWindow;
        public static Controller ControllerInstance;
    }
}
