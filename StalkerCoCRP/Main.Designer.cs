﻿namespace Taconator.CoCRP
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LogBox = new System.Windows.Forms.ListBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.debugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitRPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitRPCoCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // LogBox
            // 
            this.LogBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LogBox.FormattingEnabled = true;
            this.LogBox.Location = new System.Drawing.Point(0, 24);
            this.LogBox.Name = "LogBox";
            this.LogBox.Size = new System.Drawing.Size(562, 414);
            this.LogBox.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.debugToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(562, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // debugToolStripMenuItem
            // 
            this.debugToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitRPToolStripMenuItem,
            this.exitRPCoCToolStripMenuItem});
            this.debugToolStripMenuItem.Name = "debugToolStripMenuItem";
            this.debugToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.debugToolStripMenuItem.Text = "&Debug";
            // 
            // exitRPToolStripMenuItem
            // 
            this.exitRPToolStripMenuItem.Name = "exitRPToolStripMenuItem";
            this.exitRPToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.exitRPToolStripMenuItem.Text = "Exit &RP";
            this.exitRPToolStripMenuItem.Click += new System.EventHandler(this.exitRPToolStripMenuItem_Click);
            // 
            // exitRPCoCToolStripMenuItem
            // 
            this.exitRPCoCToolStripMenuItem.Name = "exitRPCoCToolStripMenuItem";
            this.exitRPCoCToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.exitRPCoCToolStripMenuItem.Text = "&Exit RP and CoC";
            this.exitRPCoCToolStripMenuItem.Click += new System.EventHandler(this.exitRPCoCToolStripMenuItem_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(562, 438);
            this.Controls.Add(this.LogBox);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Main";
            this.ShowIcon = false;
            this.Text = "Call of Chernobyl RP Console";
            this.Load += new System.EventHandler(this.Main_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox LogBox;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem debugToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitRPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitRPCoCToolStripMenuItem;
    }
}

